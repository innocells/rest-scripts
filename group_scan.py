import logging, sys 
from jira import JIRA

logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.DEBUG)
logging.debug("Starting....")

def main(PAT):
    jira = open_connection("https://innocells.atlassian.net", PAT)
    scan_projects(jira)

def open_connection(host, pat):
    logging.debug(f'Opening connection to {host}')
    options = {
        'server': host,
        'headers': { 'Authorization':f'Basic {pat}' }
    }
    jira = JIRA(options)
    logging.info(f'Connected to {host}')
    return jira

def scan_projects(jira):
    logging.debug('Starting projects scan.')
    for project in jira.projects():     
        logging.debug(project)
    logging.info("Scanning...")

main(sys.argv[1])