import sys
import logging
from jira import JIRA

BLOCK_SIZE = 50
ENV="uat-innocells"
PROJECT = "BSDROAD"

logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)
logging.info('This will get logged')

def main(PAT: str):
    host=f'https://{ENV}.atlassian.net'
    headers = JIRA.DEFAULT_OPTIONS["headers"].copy()
    headers["Authorization"] = f"Basic {PAT}"
    jira=JIRA(server=host, options={"headers": headers})

    moveVersionSolicitada(jira)
    updateFixVersions(jira)

def moveVersionSolicitada(jira):
    jql = f"project = '{PROJECT}' AND 'Version solicitada.' IS NOT EMPTY ORDER BY key"
    block_num = 0
    while True:
        start_idx = block_num*BLOCK_SIZE
        issues = jira.search_issues(jql, start_idx, BLOCK_SIZE)
        if len(issues) != 0:
            block_num += 1
            for issue in issues:
                if len(issue.fields.customfield_10068)==1:
                    version = issue.fields.customfield_10068[0]

                    if (issue.fields.customfield_10262 == None):
                        updateVersion = True
                    else:
                        if (ENV=="uat-innocells"):
                            updateVersion = (issue.fields.customfield_10262[0].name != version.value)
                        else:
                            updateVersion = (issue.fields.customfield_10262[0].name != version.value)
                    
                    if (updateVersion): 
                        logging.info (f'Updating: {issue}')            
                        newVersion = [] 
                        newVersion.append({'name': version.value})
                        issue.update(fields={'customfield_10262': newVersion})
                else:
                    logging.warning (f'Multiple Version Solicitada.: {issue}')  
        else:
            break



def updateFixVersions(jira):
    jql = f"project = '{PROJECT}' AND 'Version planificada' IS NOT EMPTY ORDER BY key"
    block_num = 0
    while True:
        start_idx = block_num*BLOCK_SIZE
        issues = jira.search_issues(jql, start_idx, BLOCK_SIZE)
        if len(issues) != 0:
            block_num += 1
            for issue in issues:
                if (issue.fields.customfield_10138 != None):
                    if len(issue.fields.fixVersions)==0:

                        if len(issue.fields.customfield_10138)==1:
                            version=issue.fields.customfield_10138[0]
                            logging.info (f'Updating: {issue}')            
                            fixVersions = []
                            fixVersions.append({'name': version.value})
                            issue.update(fields={'fixVersions': fixVersions})
                        else:
                            logging.warning (f'Multiple Version Planificada versions: {issue}')  
                    else:
                        if issue.fields.fixVersions[0].name != issue.fields.customfield_10138[0].value:
                            logging.warning (f'Different fixVersion/s assigned: {issue}')  
        else:
            break


main(sys.argv[1])