import sys
import logging
from jira import JIRA
from common_library import named_args

BLOCK_SIZE = 50
PROJECT = "PMOEMPR"

logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)
logging.info('Starting the script')

def main(host:str, PAT: str):
    headers = JIRA.DEFAULT_OPTIONS["headers"].copy()
    headers["Authorization"] = f"Basic {PAT}"

    logging.info('Connecting to %s', host)
    jira=JIRA(server=host, options={"headers": headers})

    logging.info('Loading epics')
    epics = get_JQL(jira,f"project = '{PROJECT}' AND issuetype IN ('Epic')")

    #logging.info('Loading tasks')
    #tasks = get_JQL(jira,f"project = '{PROJECT}' AND issuetype IN ('Timetracking') AND STATUS IN (Done, Closed)")
    #correction_1(jira, epics, tasks)

    logging.info('Loading tasks')
    tasks = get_JQL(jira,f"project = '{PROJECT}' AND issuetype IN ('Timetrackings') AND STATUS IN (Done, Closed)")
    correction_2(jira, epics, tasks)


def correction_1(jira, epics, tasks):
    logging.info('Starting correction')
    for key, task in tasks.items():
        if task.fields.parent.key in epics:
            fields = {}
            parent = epics[task.fields.parent.key]
         
            if parent.fields.duedate: # Due Date
                if not task.fields.duedate:
                    fields['duedate'] = parent.fields.duedate
                #elif parent.fields.duedate != task.fields.duedate:
                #    fields['duedate'] = parent.fields.duedate

            if parent.fields.customfield_10027: # Start Date
                if not task.fields.customfield_10027:
                    fields['customfield_10027'] = parent.fields.customfield_10027
                #elif parent.fields.customfield_10027 != task.fields.customfield_10027:
                #    fields['customfield_10027'] = parent.fields.customfield_10027

            if task.fields.customfield_10321 != parent.key: # Set BSUID to store parent
                fields['customfield_10321'] = parent.key

            if len(fields) > 0 :
                logging.info(key)
                task.update(fields)

    logging.info('Correction finish')


def correction_2(jira, epics, tasks):
    logging.info('Starting correction')
    for key, task in tasks.items():
        if task.fields.customfield_10321 in epics:
            # Search parent in BSUID.
            if  not task.fields.customfield_10008 :
                logging.info(key)
                task.update(fields={'customfield_10008': task.fields.customfield_10321})



def get_JQL(jira, jql):
    response = {}
    block_num = 0
    while True:
        start_idx = block_num*BLOCK_SIZE
        issues = jira.search_issues(jql, start_idx, BLOCK_SIZE)
        if len(issues) != 0:
            block_num += 1
            for issue in issues:
                response[issue.key] = issue
        else:
            break
    return response

if __name__ == "__main__":
    import sys
    cmd = named_args(sys.argv)
    main(cmd['host'], cmd['pat'])