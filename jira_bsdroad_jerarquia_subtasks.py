""" Script para recrear la jerarquia entre BSDROAD y HDEV """
import sys
import logging
from jira import JIRA
from common_library import named_args

BLOCK_SIZE = 50

logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)
logging.info('This will get logged')


def main(host:str,  pat: str):
    """ Main routine """
    headers = JIRA.DEFAULT_OPTIONS["headers"].copy()
    headers["Authorization"] = f"Basic {pat}"
    logging.info("Connected to %s", host)
    jira=JIRA(server=host, options={"headers": headers})
    #store_tracking_task_parent_epic(jira)
    set_parent_task_to_subtask(jira)


def store_tracking_task_parent_epic(jira):
    """ convert atributes into relationships """
    jql = "project=BSDROAD AND issuetype in ('Timetracking.')"
    block_num = 0
    while True:
        start_idx = block_num*BLOCK_SIZE
        issues = jira.search_issues(jql, start_idx, BLOCK_SIZE)
        if len(issues) != 0:
            count = 0 + (block_num * BLOCK_SIZE)
            block_num += 1
            for issue in issues:
                count += 1
                logging.info("%s <- %s", issue.fields.customfield_10008, issue.key)
                logging.info ("Progress %s of %s (%d%%)", count, issues.total, count / issues.total * 100)
                issue.update(fields={'customfield_10404': issue.fields.customfield_10008})
        else:
            break

def set_parent_task_to_subtask(jira):
    """ convert atributes into relationships """
    jql = "project=BSDROAD AND issuetype in ('Timetracking') AND cf[10404] IS NOT EMPTY"
    block_num = 0
    while True:
        start_idx = block_num*BLOCK_SIZE
        issues = jira.search_issues(jql, start_idx, BLOCK_SIZE)
        if len(issues) != 0:
            count = 0 + (block_num * BLOCK_SIZE)
            block_num += 1
            for issue in issues:
                count += 1
                logging.info("%s <- %s", issue.fields.customfield_10008, issue.key)
                logging.info ("Progress %s of %s (%d%%)", count, issues.total, count / issues.total * 100)
                #issue.update(fields={'customfield_10009': issue.fields.customfield_10404})
                issue.update(fields={'customfield_10009': "BSDROAD-1895"})
        else:
            break


if __name__ == "__main__":
    cmd = named_args(sys.argv[1:])
    main(cmd['host'], cmd['pat'])
