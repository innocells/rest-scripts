""" Library of common usefull routines """

def named_args(argv, case_sensitive=False):
    """ Convert command line arguments into a dictionary """
    result = {}
    argv = argv[1:]
    opts = [opt for opt in argv if opt.startswith("--")]
    args = [arg for arg in argv if not arg.startswith("--")]

    for i in range(0, len(opts)): # pylint: disable=consider-using-enumerate
        if case_sensitive :
            result[opts[i][2:]] = args[i]
        else:
            result[opts[i][2:].lower()] = args[i]

    result[''] = argv[-1:][0]
    return result

