""" Script to remove resolution field from reopend issues """
import sys
import logging
from jira import JIRA
from common_library import named_args

logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)
logging.info('This will get logged')

def main(server_url: str, personal_access_token: str, project: str):
    """" Main routine to execute the script """
    headers = JIRA.DEFAULT_OPTIONS["headers"].copy()
    headers["Authorization"] = f"Basic {personal_access_token}"

    jira=JIRA(server=server_url, options={"headers": headers})

    jql=f'project={project} AND resolution IS NOT EMPTY AND status NOT IN (descartada, done)'
    search=jira.search_issues(jql)

    for issue in search:
        logging.info(issue)
        issue.update(fields={'resolution': None})


if __name__ == "__main__":
    cmd = named_args(sys.argv[1:])
    main(cmd['host'], cmd['pat'], sys.argv[-1:][0])
