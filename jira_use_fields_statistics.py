'''
Genera estadisticas sobre el uso de los campos que tiene un proyecto.
'''
import json
import urllib3
from common_library import named_args
HTTP = urllib3.PoolManager()

prefilled_fields = {'customfield_10000':'35'}


def main(server_url: str, personal_access_token: str, project: str):
    ''' Rutina principal '''
    jql_url = f"{server_url}/rest/api/latest/search?fields=id&jql=project='{project}' AND created>=-365d AND Workflow  > 4 "
    
    fields_url = f"{server_url}/rest/api/latest/issue/createmeta?expand=projects.issuetypes.fields&issuetypeIds=10000&projectKeys={project}"
    fields_schema = get_raw(personal_access_token, fields_url)
    fields_epic = fields_schema['projects'][0]['issuetypes'][0]['fields']
    total_issues = get_raw(personal_access_token,f"{jql_url}")

    # encabezado
    print ("id_campo;nombre_campo;uso;sin uso;ratio de uso")

    for item in fields_epic.items():
        if 'system' not in item[1]['schema']:
            field_name = f"cf[{item[1]['schema']['customId']}]" if 'custom' in item[1]['schema'] else item[1]['schema']['system']

            # casos genericos.
            if item[1]['hasDefaultValue'] is False or item[1]['schema']['type'] in ("user"):
                field_check = "IS EMPTY"
            elif item[1]['schema']['type'] in ("option"):
                field_check = f"IN ('{item[1]['defaultValue']['value']}')"
            elif item[1]['schema']['type'] in ("priority"):
                field_check = f"IN ('{item[1]['defaultValue']['name']}')"
            else:
                field_check = f"~'{item[1]['defaultValue']}'"

            # casos especiales
            if field_name == "cf[10071]":
                field_check = "~ 'Introduce la dependencia aquí'"
            elif field_name == "cf[10142]":
                field_check = "~ ': <nombre dependencia>'"
            elif field_name == "cf[10136]":
                field_check = "~ 'Introduce la acción aquí'"
            elif field_name == "cf[10202]":
                field_check = "~ 'Introduce la acción aquí'"


            jql = f"{jql_url} AND ({field_name} {field_check} OR {field_name} IS EMPTY)"
            using_field = get_raw(personal_access_token, jql)

            if 'errorMessages' in using_field:
                print(f'{item[0]}; "ERROR: {using_field["errorMessages"][0]}"')
            else:
                percent = str(round(1-(using_field['total']/total_issues['total']), 2)).replace(".",",") # Spanish LOCALE
                print(f"{item[0]};'{item[1]['name']}';{total_issues['total']-using_field['total']};{using_field['total']};{percent}")

    return True

def get_raw(access_token, full_url, method="GET"):
    ''' Devuelve una consulta arbitraria sobre el servidor '''
    headers = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": f"Basic {access_token}"
    }

    response = HTTP.request(
        method=method,
        url=full_url,
        headers=headers,
        retries = False
    )

    return json.loads(response.data)

if __name__ == "__main__":
    import sys
    cmd = named_args(sys.argv)
    main(cmd['host'], cmd['pat'], sys.argv[-1:][0])
