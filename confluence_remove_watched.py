import logging, sys
import time
from pyfluence.pyfluence import Confluence

def main(PAT):
    logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)
    logging.info("Starting....")

    # First create a Confluence instance:
    confluence = Confluence("https://innocells.atlassian.net/wiki", PAT)
    watcher = "5fb3ac039592df00768bf1d8"
    search_and_remove_watched(confluence, f"type='page' AND watcher in ('{watcher}')", watcher)
    
    logging.info("Finish....")


def search_and_remove_watched(confluence: Confluence, search_string: str, watcher: str):
    logging.info(f"Searching pages watched by '{watcher}'")
    found_pages = confluence.search(search_string)
    logging.info(f"Found {found_pages['size']} matching pages.") 
    counter = 0

    for page in found_pages['results']:
        counter += 1
        logging.info(f"{counter}/{found_pages['size']}: Removing watcher from {page['content']['id']}: {page['content']['title']}")
        confluence.remove_watcher(page['content']['id'], watcher)

main(sys.argv[1])