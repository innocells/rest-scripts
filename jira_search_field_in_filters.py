""" Script to search all filters that are using a field in they JQL """
import logging
from jira import JIRA

logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)

def jira_search_field_in_filters(server: str, personal_access_token: str, search_str: str):
    """ Script main routine to handle arguments and call sub-processes """
    logging.debug("Starting....")
    headers = JIRA.DEFAULT_OPTIONS["headers"].copy()
    headers["Authorization"] = f"Basic {personal_access_token}"
    jira=JIRA(server=server, options={"headers": headers})
    for found_filter in search_filters_containing_field(jira, search_str):
        print(found_filter)
    logging.debug("Finish....")

def search_filters_containing_field(jira: JIRA, search_str: str):
    """ Return all filters that contains search_str in its JQL """
    affected = []
    # pylint: disable=protected-access
    filters = jira._get_json('filter') # The library don't implements search for all filters.
    for found_filter in filters:
        if found_filter['jql'].find(search_str) > 0:
            affected.append(found_filter['viewUrl'])
    return affected

def named_args(argv):
    """ Convert command line arguments into a dictionary """
    result = {}
    opts = [opt for opt in argv if opt.startswith("--")]
    args = [arg for arg in argv if not arg.startswith("--")]

    for i in range(0, len(opts)):
        result[opts[i][2:].lower()] = args[i]

    return result


if __name__ == "__main__":
    import sys
    cmd = named_args(sys.argv[1:])
    jira_search_field_in_filters(cmd['host'], cmd['pat'], sys.argv[-1:][0])




