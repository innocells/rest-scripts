import os
import json
import sys
import csv
import logging
from jira import JIRA
from jira.resources import Issue

BLOCK_SIZE = 50

logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)
logging.info('This will get logged')

def main():
    settings = carrega_configuracio_configuració()
    sincronitza_imputacions (settings['host'], settings['pat'], os.path.splitext(os.path.split(settings['archivo'])[1])[0], settings['archivo'])

def carrega_configuracio_configuració():
    settings = {}
    file_path = os.path.realpath(__file__).replace(".py",".json")
    with open(file_path, 'r',encoding='utf8') as json_file:
        for key,value in json.loads(json_file.read()).items():
            settings[key] = value
    return settings

def sincronitza_imputacions(host: str, personal_access_token: str, serial_code: str, path_arxiu_csv: str):
    host=f'https://{host}.atlassian.net'
    headers = JIRA.DEFAULT_OPTIONS["headers"].copy()
    headers["Authorization"] = f"Basic {personal_access_token}"
    jira=JIRA(server=host, options={"headers": headers})
    processa_arxiu_csv(jira, serial_code, path_arxiu_csv)

def processa_arxiu_csv(jira: JIRA, serial_code: str, path_arxiu_csv: str):
    logging.info('Processing file %s', path_arxiu_csv)
    with open(path_arxiu_csv, mode ='r', encoding="utf-8") as file:
        for line in csv.DictReader(file, delimiter=';'):
            logging.debug(line)
            issues = jira.search_issues(f"issuetype IN (Timetrackings) AND 'HDEV-Entrega-PJ' ~ '{line['Project']}'")
            if issues.total == 1:
                actualitza_dades_worklog(jira, serial_code, line, issues[0])
            elif issues.total > 1:
                logging.warning('El proyecto %s esta associado a dos o más PMOEMPR', line['Project'] )
            else:
                logging.warning('El proyecto %s no esta associado a ningún PMOEMPR', line['Project'])

def actualitza_dades_worklog(jira: JIRA, serial_code: str, line: dict, issue: Issue):
    logging.info('Actualizando datos de %s con worklogs del proyecto %s (%s)', issue.key, line['Project'], line['Total'])
    comment_id = f"Automation@{serial_code}"
    for worklog in jira.worklogs(issue):
        if worklog.comment == comment_id:
            logging.info('Eliminando worklog %s existente para %s', comment_id, issue.key)
            worklog.delete(adjustEstimate="leave")
    jira.add_worklog(issue, timeSpent=line['Total'], comment=comment_id)

if __name__ == '__main__':
    try:
        main ()
    except:
        print (sys.exc_info()[0])
        import traceback
        print (traceback.format_exc())
        print ("Press Enter to continue ..." )
        input() 