""" Script para recrear la jerarquia entre BSDROAD y HDEV """
import sys
import logging
from jira import JIRA
from common_library import named_args

BLOCK_SIZE = 50

logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)
logging.info('This will get logged')


def main(host:str,  pat: str):
    """ Main routine """
    headers = JIRA.DEFAULT_OPTIONS["headers"].copy()
    headers["Authorization"] = f"Basic {pat}"
    jira=JIRA(server=host, options={"headers": headers})
    correct_haley_parent(jira)


def correct_haley_parent(jira):
    """ convert atributes into relationships """
    jql = "project=BSDROAD " \
          "AND issuetype in ('Funcionalidad BSM', 'Funcionalidad BSO', 'Funcionalidad BSW')"
    block_num = 0
    while True:
        start_idx = block_num*BLOCK_SIZE
        issues = jira.search_issues(jql, start_idx, BLOCK_SIZE)
        if len(issues) != 0:
            count = 0 + (block_num * BLOCK_SIZE)
            block_num += 1
            for issue in issues:
                count += 1
                # Set issue parent
                parent_set = set_parent_of_issue(issue,  issue.fields.customfield_10051)
                # Set issue grand parent
                if parent_set:
                    set_parent_of_issue(jira.issue(issue.fields.customfield_10051), get_grandpad_key(jira, issue.fields.customfield_10050))
                # update progress
                logging.info ("Progress %s of %s (%d%%)", count, issues.total, count / issues.total * 100)
        else:
            break

def set_parent_of_issue(issue, parent):
    """ Update the parent of an issue if is not equal """
    set_parent = True
    if hasattr(issue.fields.customfield_10009, 'data'):
        set_parent = issue.fields.customfield_10009.data.key != parent

    if set_parent:
        logging.info ('Setting parent of %s to %s', issue.key, parent)
        issue.update(fields={'customfield_10009': parent})
 
    return set_parent

def get_grandpad_key(jira, grandpa_reference):
    """ Search an initiative based on the Jira-HDEV value """
    search = jira.search_issues(f"project = 'BSDROAD' AND TYPE=Iniciativa AND cf[10051] ~ '{grandpa_reference}'")
    if search.total > 0:
        return search[0].key

if __name__ == "__main__":
    cmd = named_args(sys.argv[1:])
    main(cmd['host'], cmd['pat'])
