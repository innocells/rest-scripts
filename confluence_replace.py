import logging, sys
import time
from pyfluence.pyfluence import Confluence

def main(PAT):
    logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)
    logging.info("Starting....")

    # First create a Confluence instance:
    confluence = Confluence("https://innocells.atlassian.net/wiki", PAT)

    # Search and replace on it.
    search_and_replace(confluence, "space!='KT' AND space!='DMKT' AND type='page' AND (title ~ BRIOPRD OR text ~ BRIOPRD)", ["BRIOPRD","BSDROAD"])

    logging.info("Finish....")

def search_and_replace(confluence, search_string: str, replace_array):
    logging.info(f"Searching '{search_string}'' and replace '{replace_array}'.")
    found_pages = confluence.search(search_string)
    logging.info(f"Found {found_pages['size']} matching pages.") 
    counter = 0

    for page in found_pages['results']:
        counter += 1

        content = confluence.get_content(page['content']['id'], expand=("space", "body.storage", "version", "container"))
        logging.info(f"{counter}/{found_pages['size']}: {content['_links']['base']}{content['_links']['webui']}")

        title = content['title']
        body = content['body']['storage']['value']

        # Change both values in a single transaction.
        if replace_array[0] in title and replace_array[0] in body:
            content['title'] = title.replace(replace_array[0], replace_array[1])
            confluence.update_content_direct(content, html_markup=body.replace(replace_array[0], replace_array[1]))
        # Change only title.
        elif replace_array[0] in title:
            confluence.update_title_direct(content, title=title.replace(replace_array[0], replace_array[1]) )
        # Change only body.
        elif replace_array[0] in body:
            confluence.update_content_direct(content, html_markup=body.replace(replace_array[0], replace_array[1]))
        
        time.sleep(0.25)
    return 


main(sys.argv[1])