import sys, logging, io
import pdfkit
from pyfluence.pyfluence import Confluence

logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)
logging.info("Starting....")


def main(PAT):
    # First create a Confluence instance:
    confluence = Confluence("https://innocells.atlassian.net/wiki", PAT)
    content = confluence.get_content("483721572", expand=("body", "body.styled_view"))

    with io.open('sample.html','w',encoding='utf8') as f:
        f.write(content['body']['styled_view']['value'])

    pdfkit.from_file('sample.html', 'sample.pdf')



main(sys.argv[1])