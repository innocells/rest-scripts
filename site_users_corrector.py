"""
Script to process Atlassian Site export and list inactive users with active account.
"""
import sys
import datetime
from datetime import datetime

def main(max_age: int, filename: str):
    """ Main routine

    Parameters
    ----------
    max_age : int
        Inactivity threshold.
    filename : str
        File in csv format (atlassian users export) to read.
    """
    # Replacements source (it will be changed by position number in array)
    lookfor = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]

    # Prepare exclusion list readed by a file list
    exclusions = {}
    with open('./site_users_corrector_exclusion.txt', 'r', encoding='utf-8') as exclusions_file:
        for line in exclusions_file:
            exclusions[line.strip()] = True

    # Using readlines() to catch the file line by line
    file1 = open(filename, 'r', encoding="utf8")
    lines = file1.readlines()
    print (lines[0].strip().replace(',',';')) # Print header

    # Iterate and process data line by line
    for line in lines[1:]:
        # Sanitize dates.
        line = line.strip()
        for i in range(0, len(lookfor)):
            line = line.replace(f' {lookfor[i]} ', f'/{i+1}/' )

        # Evaluate activity.
        fields = line.split(",")
        active = False
        active = active or ( datesdiffnow(fields[4]) < max_age )
        active = active or ( datesdiffnow(fields[6]) < max_age )
        active = active or ( datesdiffnow(fields[7]) < max_age )

        # Print inactive accounts candidates (if not excluded).
        if fields[2] not in exclusions:
            if not active and fields[3] == 'Active' and fields[5] != 'Site Admin':
                print(line.replace(',',';'))
            elif max_age<0 : # Negative max_age show everything.
                print(line.replace(',',';'))


def datesdiffnow(dt_str: str):
    """ Calculate number of days since dt_str to now.

    Parameters
    ----------
    dt_str : str
        Date in string format to compare with now.

    Returns
    -------
    integer
        Number of days
    """
    try:
        # Calculate number of days since dt_str to now.
        dt_str = dt_str if dt_str != 'Never logged in' else "01/01/1900"
        return (datetime.now() - datetime.strptime(dt_str, '%d/%m/%Y')).days

    # Catch exceptions
    except Exception as error:
        return datesdiffnow("01/01/1900")

main(int(sys.argv[1]), sys.argv[2])

# python site_users_corrector.py 45 "C:\Users\sicb\OneDrive\OneDrive - GFT Technologies SE\Baixades\export-users.csv" > "C:\Users\sicb\OneDrive\OneDrive - GFT Technologies SE\Baixades\output.csv"
